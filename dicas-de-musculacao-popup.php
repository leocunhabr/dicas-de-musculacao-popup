<?php
/*
Plugin Name: Dicas de Musculação - Popup
Plugin URI:
Description: Plugin de popup com personalização de links e imagens.
Version: 1.2.4
Author: Leonardo Cunha
Author URI: https://twitter.com/LEO__o
*/

//PADRÃO
add_action('admin_menu', 'my_menu_pages');

function my_menu_pages(){
  add_menu_page('My Page Title', 'Popup Segmentado', 'manage_options', 'popup-segmentado', 'fomulario_padrao', $icon_url = 'dashicons-forms', $position = null );
}

function fomulario_padrao(){
?>
  <div class="_pop-gif" style="margin-left:20px;width:45%;">
    <h1>Admin Popup Segmentado</h1>
    <form name="post" action="options.php" method="post" id="post">
      <?php settings_fields( 'popup_padrao_settings' ); ?>
      <?php do_settings_sections( 'popup_padrao_settings' ); ?>
        <br/>

          <h2>Link Padrão</h2>

          <p>
            <label for="link_padrao_popup">Link do produto</label>
            <input type="text" name="link_padrao_popup" id="link_padrao_popup" value="<?php echo esc_attr( get_option('link_padrao_popup') ); ?>" style="width:100%;" placeholder="url"/>
          </p>

          <br/>
          <svg width="100%" height="3">
            <rect x="0" y="0" width="100%" height="3" fill="#f7f7f7"/>
          </svg>
          <br/><br/>

          <h2>Banner Fixo</h2>

          <p>
            <label for="_imagem_produto_popup">Link da imagem do produto</label>
            <input type="text" name="imagem_padrao_popup" id="_imagem_produto_popup" value="<?php echo esc_attr( get_option('imagem_padrao_popup') ); ?>" style="width:100%;" placeholder="url"/>
          </p>

          <br/>
          <svg width="100%" height="3">
            <rect x="0" y="0" width="100%" height="3" fill="#f7f7f7"/>
          </svg>
          <br/><br/>

            <h2>Banner Animado</h2>
              <p>
                <label for="nome_padrao_popup">Nome do Produto</label>
                <input type="text" name="nome_padrao_popup"   id="nome_padrao_popup" value="<?php echo esc_attr( get_option('nome_padrao_popup') ); ?>" style="width:100%;" placeholder="url"/>
              </p>
              <p>
                <label for="preco_padrao_popup">Preço do Produto</label>
                <input type="text" name="preco_padrao_popup"  id="preco_padrao_popup" value="<?php echo esc_attr( get_option('preco_padrao_popup') ); ?>" style="width:100%;" placeholder="url"/>
              </p>
              <p>
                <label for="imagem_padrao_popup_produto">Imagem do Produto</label>
                <input type="text" name="imagem_padrao_popup_produto" id="imagem_padrao_popup_produto" value="<?php echo esc_attr( get_option('imagem_padrao_popup_produto') ); ?>" style="width:100%;" placeholder="url"/>
              </p>

              <br/>
              <svg width="100%" height="3">
                <rect x="0" y="0" width="100%" height="3" fill="#f7f7f7"/>
              </svg>
              <br/><br/>

              <h2>Tempo de Abertura</h2>
              <p>
                <label for="time_popup_segmentado">Tempo em milesegundos</label>
                <input type="text" name="time_popup_segmentado"   id="time_popup_segmentado" value="<?php if(get_option('time_popup_segmentado') != ''){
                  echo get_option('time_popup_segmentado');
                } ?>" style="width:100%;" placeholder="time"/>
              </p>

            <p>
              <?php echo submit_button(); ?>
            </p>
    </form>
  </div>
<?php
}

//*******
    add_action( 'admin_init', 'popup_padrao_settings' );

    function popup_padrao_settings()
    {
      register_setting( 'popup_padrao_settings', 'imagem_padrao_popup' );
      register_setting( 'popup_padrao_settings', 'link_padrao_popup' );
      register_setting( 'popup_padrao_settings', 'nome_padrao_popup' );
      register_setting( 'popup_padrao_settings', 'preco_padrao_popup' );
      register_setting( 'popup_padrao_settings', 'imagem_padrao_popup_produto' );
      register_setting( 'popup_padrao_settings', 'time_popup_segmentado' );
    }

//*****
function popup_style() {
    //wp_enqueue_style( 'style-ddm-pop', plugin_dir_url( __FILE__ )."assets/style-ddm-pop.css" );
    //wp_enqueue_style( 'style-growth',  plugin_dir_url( __FILE__ ). "assets/style-growth.css");
    //wp_enqueue_script('jquery');
    echo '<style>a.imagem img{border-radius:10px;width:100%}.ddm-popup{max-width:100%;width:600px;height:auto;position:fixed;z-index:999999;margin:0 auto;top:25vh;left:0;right:0}.box-close-popup{position:absolute;width:100%}.box-close-popup-dois{width:100%}#fundo-ddm-popup{width:100%;height:100%;position:fixed;background-color:rgba(0,0,0,.8);z-index:999998;top:0;right:0}#close-popup{position:absolute;right:10px;top:10px;max-width:30px;z-index:9999;cursor:pointer}.link-popup{position:fixed;width:100%;height:100%;z-index:99999}.ddm-popup img{height:auto;}</style>';
}
add_action( 'wp_enqueue_scripts', 'popup_style' );


//ADICIONANDO O META BOX
add_action( 'add_meta_boxes', 'vinil_meta_box_add' );
function vinil_meta_box_add()
{
add_meta_box( 'my-meta-box-id', 'Personalização do Popup', 'vinil_meta_box_vinil', 'post', 'normal', 'high' );
}

//FORMULARIO PARA SALVAR OS DADOS
function vinil_meta_box_vinil()
{

//===== CHECKBOX ======
  global $post;
  $custom_checkbox = get_post_meta($post->ID, 'custom_input', true);
  if ($custom_checkbox == 'yes') $custom_checkbox_val = 'checked="checked"';

//===== inputs ======
$text       = get_post_meta($post->ID, '_texto_popup', true);
$imagem     = get_post_meta($post->ID, '_imagem_popup', true);
$link_atras = get_post_meta($post->ID, '_link_popup', true);

$nome_produto_popup   = isset( $values['_nome_produto_popup'] )    ? esc_attr( $values['_nome_produto_popup'][0] )    : '';
$preco_produto_popup  = isset( $values['_preco_produto_popup'] )   ? esc_attr( $values['_preco_produto_popup'][0] )   : '';
$imagem_produto_popup = isset( $values['_imagem_produto_popup'] )  ? esc_attr( $values['_imagem_produto_popup'][0] )  : '';

wp_nonce_field( 'my_meta_box_nonce', 'meta_box_nonce' );

//Form dentro de cada post
?>
<div style="display:flex;justify-content:space-around;flex-wrap:wrap;">
  <div class="_pop-png" style="width:45%;"><h1>Banner Estatico</h1>
    <p>
      <label for="_imagem_popup">Url da imagem</label>
      <input type="text" name="_imagem_popup" id="_imagem_popup" value="<?= $values['_imagem_popup'][0]; ?>" style="width:100%;"/>
    </p>
    <p>
      <label for="checkbox_popup">Não utilizar popup neste post.</label><br>
      <?php echo '<input type="checkbox" name="custom_input" id="custom_input" value="yes" '.$custom_checkbox_val.' />'; ?>
    </p>
  </div>
  <div class="_pop-gif" style="margin-left:20px;width:45%;">
    <h1>Banner Animado</h1>
      <p>
        <label for="_nome_produto_popup">Nome do Produto</label>
        <input type="text" name="_nome_produto_popup" id="_nome_produto_popup" value="<?= $values['_nome_produto_popup'][0]; ?>" style="width:100%;"/>
      </p>
      <p>
        <label for="_preco_produto_popup">Preço do Produto</label>
        <input type="text" name="_preco_produto_popup" id="_preco_produto_popup" value="<?= $values['_preco_produto_popup'][0]; ?>" style="width:100%;"/>
      </p>
      <p>
        <label for="_imagem_produto_popup">Imagem do Produto</label>
        <input type="text" name="_imagem_produto_popup" id="_imagem_produto_popup" value="<?= $values['_imagem_produto_popup'][0]; ?>" style="width:100%;"/>
      </p>
    </div>
</div>

<br/>
<svg width="100%" height="3">
  <rect x="0" y="0" width="100%" height="3" fill="#f7f7f7"/>
</svg>
<br/><br/>

<h1>Link Fixos</h1>
<div class="_pop-gif" style="display:flex;justify-content:center;flex-wrap:wrap;">
<p style="width:45%;">
  <label for="_texto_popup">Link para ir ao site</label>
  <input type="text" name="_texto_popup" id="_texto_popup" value="<?= $values['_texto_popup'][0]; ?>" style="width:80%;"/>
</p>
<p style="width:45%;margin-left:20px;">
  <label for="_link_popup">Link atraz do popup</label>
  <input type="text" name="_link_popup" id="_link_popup" value="<?= $values['_link_popup'][0]; ?>" style="width:80%;"/>
</p>
</div>

<?php
}

add_action( 'save_post', 'vinil_meta_box_save' );
function vinil_meta_box_save( $post_id )
{

if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'my_meta_box_nonce' ) ) return;

if( !current_user_can( 'edit_post' ) ) return;

$allowed = array('a' => array('href' => array()));

if( isset( $_POST['_texto_popup'] ) )
update_post_meta( $post_id, '_texto_popup', wp_kses( $_POST['_texto_popup'], $allowed ) );

if( isset( $_POST['_imagem_popup'] ) )
update_post_meta( $post_id, '_imagem_popup', wp_kses( $_POST['_imagem_popup'], $allowed ) );

if( isset( $_POST['_link_popup'] ) )
update_post_meta( $post_id, '_link_popup', wp_kses( $_POST['_link_popup'], $allowed ) );

//Produto
if( isset( $_POST['_nome_produto_popup'] ) )
update_post_meta( $post_id, '_nome_produto_popup', wp_kses( $_POST['_nome_produto_popup'], $allowed ) );

if( isset( $_POST['_preco_produto_popup'] ) )
update_post_meta( $post_id, '_preco_produto_popup', wp_kses( $_POST['_preco_produto_popup'], $allowed ) );

if( isset( $_POST['_imagem_produto_popup'] ) )
update_post_meta( $post_id, '_imagem_produto_popup', wp_kses( $_POST['_imagem_produto_popup'], $allowed ) );


if( isset( $_POST['nome_padrao_popup'] ) )
update_post_meta( $post_id, 'nome_padrao_popup', wp_kses( $_POST['nome_padrao_popup'], $allowed ) );

if( isset( $_POST['preco_padrao_popup'] ) )
update_post_meta( $post_id, 'preco_padrao_popup', wp_kses( $_POST['preco_padrao_popup'], $allowed ) );

}


//Colocar div no site
function the_post_action() {
  $values = get_post_custom( $post->ID );

  //===== BOTÃO DE CHECKBOX - VERIFICAÇÃO ======//
  global $post;
  $custom_checkbox_yes = get_post_meta($post->ID, 'custom_input', true);
  if ($custom_checkbox_yes != 'yes') {
  if($values['_imagem_popup'][0] != '' && is_single())
  {
    echo '<div class="ddm-popup" id="ddm-popup" style="display:none;">
            <div class="">
              <a class="imagem" href="'.$values['_texto_popup'][0].'" target="_blank" rel="nofollow noopener" style="position: absolute;">
                <img alt="Whey-concentrado" src="'.$values['_imagem_popup'][0].'"  width="600" height="364"/>
              </a>
              <div class="box-close-popup">
                <svg version="1.1" id="close-popup" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
                  <circle style="fill:#D75A4A;" cx="25" cy="25" r="25"></circle>
                  <polyline style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-miterlimit:10;" points="16,34 25,25 34,16"></polyline>
                  <polyline style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-miterlimit:10;" points="16,16 25,25 34,34"></polyline>
                </svg>
              </div>
            </div>
          </div>
          <div id="fundo-ddm-popup" style="display:none;"></div>
          <a class="link-popup" href="'.$values['_link_popup'][0].'" target="_blank" rel="nofollow noopener" style="display:none;"></a>';
} elseif ($values['_imagem_produto_popup'][0] != '' && is_single()){
  echo '
  <div class="ddm-popup" id="ddm-popup" style="display:none;">
    <a href="'.$values['_texto_popup'][0].'" target="_blank" rel="nofollow noopener">
      <div class="box-popup">
        <div class="texto-black-popup">COMPRE DIRETO DA FABRICA</div>
        <div class="black-popup"></div>
        <div class="titulo-popup">'.$values['_nome_produto_popup'][0].'<span class="sub-titulo"></span></div>
        <div class="preco-popup">R$ '.$values['_preco_produto_popup'][0].'<br><span class="sub-titulo-popup">à vista</span></div>
        <img alt="Suplemento" src="'.$values['_imagem_produto_popup'][0].'" class="produto-popup" />
        <img alt="Logo Growth" src="https://dicasdemusculacao.com.br/wp-content/uploads/2018/05/logo-growth.png" class="logo-growth-popup" />
        <img alt="Botão de Compra" src="https://dicasdemusculacao.com.br/wp-content/uploads/2018/05/botao-de-compra-grande.png" class="botao-compra-growth-popup" />
        </div>
        </a>
  <div class="box-close-popup-dois">
    <svg version="1.1" id="close-popup" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
      <circle style="fill:#D75A4A;" cx="25" cy="25" r="25"></circle>
      <polyline style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-miterlimit:10;" points="16,34 25,25 34,16"></polyline>
      <polyline style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-miterlimit:10;" points="16,16 25,25 34,34"></polyline>
    </svg>
  </div>
  </div>
  <div id="fundo-ddm-popup" style="display:none;"></div>
  <a class="link-popup" href="'.$values['_link_popup'][0].'" target="_blank" rel="nofollow noopener" style="display:none;"></a>
  ';

}elseif(get_option( 'imagem_padrao_popup' ) != ''){

  echo '<div class="ddm-popup" id="ddm-popup" style="display:none;">
                  <div class="">
                  <a class="imagem" href="'.get_option( 'link_padrao_popup' ).'" target="_blank" rel="nofollow noopener" style="position: absolute;">
                    <img alt="Whey-concentrado" src="'.get_option( 'imagem_padrao_popup' ).'"  width="660" height="364">
                  </a>
                  <div class="box-close-popup">
                    <svg version="1.1" id="close-popup" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
                      <circle style="fill:#D75A4A;" cx="25" cy="25" r="25"></circle>
                      <polyline style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-miterlimit:10;" points="16,34 25,25 34,16"></polyline>
                      <polyline style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-miterlimit:10;" points="16,16 25,25 34,34"></polyline>
                    </svg>
                  </div>
                </div>
              </div>
          <div id="fundo-ddm-popup" style="display:none;"></div>
          <a class="link-popup" href="'.get_option( 'link_padrao_popup' ).'" target="_blank" rel="nofollow noopener" style="display:none;"></a>';
}elseif(get_option( 'nome_padrao_popup' ) != '' && get_option('imagem_padrao_popup_produto' ) != ''){
  echo '
  <div class="ddm-popup" id="ddm-popup" style="display:none;">
    <a href="'.get_option( 'link_padrao_popup' ).'" target="_blank" rel="nofollow noopener">
      <div class="box-popup">
        <div class="texto-black-popup">COMPRE DIRETO DA FABRICA</div>
        <div class="black-popup"></div>
        <div class="titulo-popup">'.get_option( 'nome_padrao_popup' ).'<span class="sub-titulo"></span></div>
        <div class="preco-popup">R$ '.get_option( 'preco_padrao_popup' ).'<br><span class="sub-titulo-popup">à vista</span></div>
        <img alt="Suplemento" src="'.get_option('imagem_padrao_popup').'" class="produto-popup" />
        <img alt="Logo Growth" src="https://dicasdemusculacao.com.br/wp-content/uploads/2018/05/logo-growth.png" class="logo-growth-popup" />
        <img alt="Botão de Compra" src="https://dicasdemusculacao.com.br/wp-content/uploads/2018/05/botao-de-compra-grande.png" class="botao-compra-growth-popup" />
        </div>
        </a>
  <div class="box-close-popup-dois">
    <svg version="1.1" id="close-popup" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
      <circle style="fill:#D75A4A;" cx="25" cy="25" r="25"></circle>
      <polyline style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-miterlimit:10;" points="16,34 25,25 34,16"></polyline>
      <polyline style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-miterlimit:10;" points="16,16 25,25 34,34"></polyline>
    </svg>
  </div>
  </div>
  <div id="fundo-ddm-popup" style="display:none;"></div>
  <a class="link-popup" href="'.get_option( 'link_padrao_popup' ).'" target="_blank" rel="nofollow noopener" style="display:none;"></a>
  ';
}else{
  echo '<div id="sem-popup"></div>';
}// if

//ADICIONANDO STYLES E SCRIPTS
function popup_scripts() {
    //wp_enqueue_script( 'script-ddm-pop', plugin_dir_url( __FILE__ )."assets/script-ddm-pop.js", array(jquery), '1.2.4');
    echo '<script>var time_popup_segmentado='.get_option('time_popup_segmentado').';</script>';
    echo '<script>var idPopup=document.querySelector("#ddm-popup"),idFundo=document.querySelector("#fundo-ddm-popup");function fadeIn(e,o){processa(e,o,0,100)}function fadeOut(e,o){processa(e,o,100,0)}function processa(e,o,n,t){0==n?(increment=2,e.style.display="block"):increment=-2,opc=n,intervalo=setInterval(function(){opc<=t&&(0==t?e.style.display="none":(opc+=increment,e.style.opacity=opc/100,e.style.filter="alpha(opacity="+opc+")"),clearInterval(intervalo))},10*o)}function setCookie(e,o,n){var t=new Date;t.setTime(t.getTime()+24*n*60*60*1e3);var i="expires="+t.toUTCString();document.cookie=e+"="+o+";"+i+";path=/"}function getCookie(e){for(var o=e+"=",n=decodeURIComponent(document.cookie).split(";"),t=0;t<n.length;t++){for(var i=n[t];" "==i.charAt(0);)i=i.substring(1);if(0==i.indexOf(o))return i.substring(o.length,i.length)}return""}function checkCookie(){""!=getCookie("check-cookie-segmentado")?console.log("Com cookie"):(console.log("Criando cookie"),setCookie("check-cookie-segmentado","V1.2.3",.5),fadeIn(idPopup,1),fadeIn(idFundo,1),console.log("Cookie Criado"))}idPopup.addEventListener("click",function(){idPopup.remove(idPopup.selectedIndex),idFundo.remove(idFundo.selectedIndex)}),idFundo.addEventListener("click",function(){idPopup.remove(idPopup.selectedIndex),idFundo.remove(idFundo.selectedIndex)});var elementoPai=document.body;setTimeout(function(){document.addEventListener("load",checkCookie())},time_popup_segmentado);
    </script>';
}

add_action( 'wp_footer', 'popup_scripts' );

}// CHECKBOX
}// function

  add_action( 'wp_footer', 'the_post_action', 1, 1 );


// //OCULTANDO META_BOX
// add_action( 'admin_menu', 'remove_custom_fields' );
//
// function remove_custom_fields()
// {
//   remove_meta_box( 'my-meta-box-id', 'post', 'normal' );
// }

//===== CHECKBOX - SAVE ======
add_action("save_post", "save_detail");

function save_detail(){
  global $post;
  if (define('DOING_AUTOSAVE', '') && DOING_AUTOSAVE) {
    return $post->ID;
  }

  update_post_meta($post->ID, "custom_input", $_POST["custom_input"]);
}
?>
