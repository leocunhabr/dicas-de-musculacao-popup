
// FADEOUT (fechando popup)
const idPopup = document.querySelector('#ddm-popup');
const idFundo = document.querySelector('#fundo-ddm-popup');

function fadeIn(element, time){
    processa(element, time, 0, 100);
}

function fadeOut(element, time){
    processa(element, time, 100, 0);
}

function processa(element, time, initial, end) {
  if (initial == 0) {
    increment = 2;
    element.style.display = "block";
  } else {
    increment = -2;
  }

  opc = initial;

  intervalo = setInterval(function() {
    if(opc <= end) {
      if (end == 0) {
        element.style.display = "none";
      } else {
        opc += increment;
        element.style.opacity = opc/100;
        element.style.filter = "alpha(opacity="+opc+")";
      }
      clearInterval(intervalo);
    }
  }, time * 10);
}

idPopup.addEventListener('click', function(){
  idPopup.remove(idPopup.selectedIndex);
  idFundo.remove(idFundo.selectedIndex);
  // fadeOut(idPopup, 1);
  // fadeOut(idFundo, 1);
});

idFundo.addEventListener('click', function(){
  idPopup.remove(idPopup.selectedIndex);
  idFundo.remove(idFundo.selectedIndex);
  // fadeOut(idPopup, 1);
  // fadeOut(idFundo, 1);
});

//COOKIE (criando cookie)
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = 'expires='+d.toUTCString();
  document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
}

//COOKIE (colocando cookie)
function getCookie(cname) {
var name = cname + '=';
var decodedCookie = decodeURIComponent(document.cookie);
var ca = decodedCookie.split(';');
for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
        c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
    }
}
return '';
}


//COOKIE (checando cookie)
function checkCookie() {
  var name = getCookie('check-cookie-segmentado'); //Nome para checar
  if (name != "") { //
    console.log('Com cookie');
  }else{
    console.log('Criando cookie');
    setCookie('check-cookie-segmentado', 'V1.2.3', 0.5); //Setando cookie do popup
    fadeIn(idPopup, 1); //Abrindo o popup
    fadeIn(idFundo, 1); //Abrindo o fundo do popup
    // $('.link-popup').fadeIn('slow'); //Abrindo o link atras do popup
    console.log('Cookie Criado');
  }
}

//LINK POPUP
var elementoPai = document.body;
// var linkPopup = document.querySelector('.link-popup');

//LINK (Carregar no carregamento da pagina)
setTimeout(function(){
  //if(!$('#sem-popup').length){
    document.addEventListener('load', checkCookie());
  //}
}, time_popup_segmentado);

//LINK (Remover link-popup)
// if (linkPopup) {
//   linkPopup.addEventListener('click', function() {
//     elementoPai.removeChild(linkPopup);
//   });
// }
